<?php
/**
 * Plugin Name: Dm3Options
 * Author: incrediblebytes
 * Author URI: http://incrediblebytes.com
 * Description: Theme options panel.
 * License: GPLv2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * Version: 1.4.0
 *
 * @package Dm3Options
 */

/*
Copyright (C) 2015 http://incrediblebytes.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

if ( ! defined( 'ABSPATH' ) ) exit;

define( 'DM3_FRAMEWORK_DIR', plugin_dir_path( __FILE__ ) );
define( 'DM3_FRAMEWORK_URL', plugins_url( '', __FILE__ ) );

/**
 * Setup admin.
 */
function dm3options_init() {
	if ( is_admin() ) {
		require_once DM3_FRAMEWORK_DIR . '/admin.php';
		require_once DM3_FRAMEWORK_DIR . '/dm3-custom-fields.php';
	}
}
add_action( 'init', 'dm3options_init' );

/**
 * Load text domain.
 */
function dm3options_textdomain() {
	load_plugin_textdomain( 'dm3-options', false, 'dm3-options/languages' );
}
add_action( 'plugins_loaded', 'dm3options_textdomain' );

if ( ! function_exists( 'dm3_option' ) ) :
/**
 * Get a theme option.
 *
 * @param $key Option key
 * @param $default Return value if option doesn't exist
 * @return mixed
 */
function dm3_option( $key = null , $default = null ) {
	static $opt = null;

	if ( $opt === null ) {
		$opt = get_option( 'dm3_fwk', array() );
	}

	if ( ! $key ) {
		return $opt;
	}

	$value = null;

	if ( isset( $opt[$key] ) ) {
		$value = $opt[$key];
	} else {
		$value = $default;
	}

	return apply_filters( 'dm3_option_filter', $value, $key );
}
endif;
