msgid ""
msgstr ""
"Project-Id-Version: Dm3Options\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-11-19 05:13-0500\n"
"PO-Revision-Date: 2015-11-19 05:13-0500\n"
"Last-Translator: incrediblebytes\n"
"Language-Team: incrediblebytes <contact@incrediblebytes.com>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c\n"
"X-Poedit-Basepath: .\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Generator: Poedit 1.5.4\n"
"X-Poedit-SearchPath-0: ..\n"

#: ../admin.php:40 ../admin.php:41 ../dm3-custom-fields.php:49
#: ../options-view.php:18
msgid "Theme Options"
msgstr ""

#: ../dm3-admin-form.php:173
msgid "Insert"
msgstr ""

#: ../dm3-admin-form.php:173
msgid "Select"
msgstr ""

#: ../options-view.php:15
msgid "Changes have been saved."
msgstr ""

#: ../options-view.php:23
msgid "Save changes"
msgstr ""
