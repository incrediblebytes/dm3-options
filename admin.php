<?php
/**
 * Initialize admin.
 *
 * @package Dm3Options
 * @since Dm3Options 1.0
 * @version 1.1
 */

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'admin_menu', 'dm3_add_theme_menu' );
add_action( 'admin_enqueue_scripts', 'dm3options_enqueue_scripts' );
add_action( 'admin_head', 'dm3_wp_head', 9999 );

/**
 * Enqueue scripts and styles.
 */
function dm3options_enqueue_scripts() {
	// Stylesheets.
	wp_enqueue_style('color-picker', DM3_FRAMEWORK_URL . '/css/colorpicker.css');
	wp_enqueue_style('dm3-functions', DM3_FRAMEWORK_URL . '/css/functions.css');

	// Media scripts.
	if ( isset( $_GET['page'] ) && ( isset($_GET['page'] ) && $_GET['page'] == 'dm3_theme_options_default' ) ) {
		wp_enqueue_media();
		wp_enqueue_script( 'dm3-select-media', DM3_FRAMEWORK_URL . '/js/dm3-select-media.js', array( 'jquery' ), '1.1', true );
	}
	
	// Other scripts.
	wp_enqueue_script('color-picker', DM3_FRAMEWORK_URL . '/js/colorpicker.js', array( 'jquery' ), '', true);
	wp_enqueue_script('dm3-functions', DM3_FRAMEWORK_URL . '/js/functions.js', array( 'jquery' ), '', true);
}

/**
 * Add admin menu.
 */
function dm3_add_theme_menu() {
	add_theme_page(
		__('Theme Options', 'dm3-options'),
		__('Theme Options', 'dm3-options'),
		'manage_options',
		'dm3_theme_options_default',
		'dm3_theme_options_default'
	);
}

/**
 * Setup framework constants for javascripts.
 */
function dm3_wp_head() {
	?>
	<script>
	dm3Framework = {
		themeUrl: '<?php echo get_template_directory_uri(); ?>',
		frameworkUrl: '<?php echo DM3_FRAMEWORK_URL; ?>'
	};
	</script>
	<?php
}

/**
 * Display the default options page.
 */
function dm3_theme_options_default() {
	$options_file = get_template_directory() . '/dm3-options/options.php';

	if ( is_readable( $options_file ) ) {
		include $options_file;
		include DM3_FRAMEWORK_DIR . '/options-view.php';
	}
}
