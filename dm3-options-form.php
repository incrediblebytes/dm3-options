<?php
/**
 * Dm3OptionsForm
 *
 * @package Dm3Options
 * @since Dm3Options 1.0
 * @version 1.1
 */

if ( ! defined( 'ABSPATH' ) ) exit;

require_once 'dm3-admin-form.php';

class Dm3OptionsForm {
	private $fields = array();
	private $options = array();
	private $categories = array();
	private $saved = null;

	/**
	 * Constructor.
	 *
	 * @param array $fields
	 * @param string $option
	 */
	public function __construct( $fields, $option ) {
		if ( is_array( $fields ) ) {
			$this->fields = $fields;
		}

		if ( is_string( $option ) && ! empty( $option ) ) {
			$this->options = get_option( $option, array() );
		}
	}

	/**
	 * Get options.
	 * 
	 * @return array
	 */
	public function getOptions() {
		return $this->options;
	}

	/**
	 * Get fields.
	 * 
	 * @return array
	 */
	public function getFields() {
		return $this->fields;
	}

	/**
	 * Get categories.
	 * 
	 * @return array
	 */
	public function getCategories() {
		return $this->categories;
	}

	/**
	 * Check if options were saved.
	 * 
	 * @return boolean
	 */
	public function isSaved() {
		return $this->saved;
	}

	/**
	 * Set option by key.
	 */
	public function setOption( $key, $value ) {
		$this->options[ $key ] = $value;
	}

	/**
	 * Save theme options.
	 * 
	 * @param string $options_name
	 */
	public function save( $options_name ) {
		if ( ! isset( $_POST['_wpnonce'] ) || ! wp_verify_nonce( $_POST['_wpnonce'], 'dm3_options' ) ) {
			return;
		}

		if ( ! current_user_can( 'manage_options' ) ) {
			return;
		}

		foreach ( $this->fields as $key => $f ) {
			if ( ! isset( $_POST[ $key ] ) ) {
				if ( $f['type'] == 'checkbox' ) {
					$this->options[ $key ] = '';
				}

				continue;
			}

			$value = $_POST[ $key ];

			if ( ! isset( $f['filter'] ) ) {
				$f['filter'] = '';
			}

			switch ( $f['filter'] ) {
				case 'int':
					$value = intval( $value );
					break;
					
				case 'none':
					if ( current_user_can( 'unfiltered_html' ) ) {
						break;
					}
					
				default:
					if ( $f['type'] == 'select' ) {
						$valid = false;

						foreach ( $f['options'] as $k => $val ) {
							if ( is_array( $val ) ) {
								foreach ( $val['options'] as $k => $val ) {
									if ( $k == $value ) {
										$valid = true;

										break;
									}
								}
							} elseif ( $k == $value ) {
								$valid = true;

								break;
							}
						}

						if ( ! $valid ) {
							$value = '';
						}
					} elseif ( ( $f['type'] == 'radios' || $f['type'] == 'pages' ) && is_array( $f['options'] ) && ! array_key_exists( $value, $f['options'] ) ) {
						$value = '';
					} elseif ( $f['type'] == 'coloroptions' ) {
						if ( ! in_array( $value, $f['options'] ) ) {
							$value = '';
						}
					} else {
						$value = esc_html($value);
					}
			}

			$this->options[$key] = $value;
		}

		$this->options = stripslashes_deep( $this->options );

		update_option( $options_name, $this->options );

		$this->saved = true;
	}

	/**
	 * Output a field.
	 * 
	 * @param string $name
	 * @param array $field
	 */
	public function showField( $name, $field ) {
		$func = 'field' . ucfirst( $field['type'] );
		$value = isset( $this->options[ $name ] ) ? $this->options[ $name ] : ( isset( $field['value'] ) ? $field['value'] : '' );

		if ( method_exists( 'Dm3AdminForm', $func ) ) {
			echo call_user_func( array( 'Dm3AdminForm', $func ), $name, $value, $field );
		}
	}

	/**
	 * Output the form.
	 */
	public function showForm() {
		$i = 1;
		?>
		<form id="dm3_form" class="dm3_form" method="post" enctype="multipart/form-data">
			<input type="hidden" name="_wpnonce" value="<?php echo wp_create_nonce( 'dm3_options' ); ?>" />
			<ul class="tabs-nav">
				<?php foreach ( $this->categories as $c ) : ?>
					<li><a href="#<?php echo $i++; ?>"><?php echo $c['label']; ?></a></li>
				<?php endforeach; ?>
			</ul>
			<div class="tabs">
				<?php $i = 1; ?>
				<?php foreach ( $this->categories as $c ) : ?>
					<div id="<?php echo $i++; ?>" class="tab">
						<div class="dm3-form-elements">
							<?php
								foreach ( $c['fields'] as $field ) {
									$this->showField( $field, $this->fields[ $field ] );
								}
							?>
						</div>
						<?php
							if ( $c['footer'] ) {
								echo $c['footer'];
							}
						?>
					</div>
				<?php endforeach; ?>
				<input type="hidden" name="cur_tab_idx" value="<?php if ( isset( $_POST['cur_tab_idx'] ) ) echo intval( $_POST['cur_tab_idx'] ); ?>">
			</div>
		</form>
		<?php
	}

	/**
	 * Group options in categories.
	 * 
	 * @param string $label
	 * @param array $fields
	 * @param string $footer
	 */
	public function addCategory( $label, $fields, $footer = null ) {
		$this->categories[] = array(
			'label'  => $label,
			'fields' => $fields,
			'footer' => $footer,
		);
	}
}
